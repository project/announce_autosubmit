# A11y Announce Form Auto-Submit

Shows an accessible announcement when a form is auto-submitted and keep focus.

When an element value changes in an auto-submitted form like Views exposed filters,
automatically refreshes the results on the page (whether is AJAX or reloading the page).
This experience is confusing for a screen reader user and they are not aware any changes have taken place.

Also the element which triggered the auto-submit loose focus (in both AJAX and page reloading cases).

This module solves this accessibility issue by checking the URL of the page being the
same, and that the form parameters have changed.

Then alerts the user that the page results have reloaded and changed, announcing what has change (a visually hidden div for screen readers only), and restoring focus.It works with Views exposed filters forms.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/announce_autosubmit).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/announce_autosubmit).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Navigate to Administration > Extend and enable the module.


## Maintainers

- Juan Olalla - [juanolalla](https://www.drupal.org/u/juanolalla)
