/**
 * @file
 * Announce that the form has been auto-submitted and restore focus.
 */
(function (Drupal, $) {
  'use strict';

  Drupal.behaviors.AnnounceFormSubmitBehavior = {
    attach: function (context, settings) {

      // For each form create a submit handler to store the form id.
      for (var formId in settings.announce_autosubmit.forms) {
        $(once('announce-' + formId, '#' + formId, context)).each(function () {
          $(this).submit(function(event) {
            window.localStorage.setItem('announce_autosubmit.submittedFormId', this.id);
          });

          // For each parameter in this form, store its label on value change,
          // so it can be used later to be announced when AJAX is complete.
          var nameLabel = {};
          for (var index in settings.announce_autosubmit.forms[formId].formParameters) {
            var formParameter = settings.announce_autosubmit.forms[formId].formParameters[index];
            $('[name="' + formParameter.name + '"]', this).each(function () {
              nameLabel[formParameter.name] = formParameter.label;
              $(this).change(function(event) {
                // Save changed item label to announce it on ajax complete.
                window.localStorage.setItem('announce_autosubmit.changedFilterLabel', nameLabel[this.name]);
              });
            });
          }
        });
      }

      $(once('announceFormSubmit', document.body)).each(function () {

        $(document).ajaxComplete(function(event, request, ajaxSettings) {
          var changedFilterLabel = window.localStorage.getItem('announce_autosubmit.changedFilterLabel');
          if (changedFilterLabel) {
            Drupal.announce('<p>' +
              Drupal.t('New "@label" filter value applied to results',
              {'@label': changedFilterLabel}) +
            '</p>');
          }
          else {
            Drupal.announce('<p>' +
              Drupal.t('The filters values have changed and applied to results.') +
              '</p>');
          }
          window.localStorage.removeItem('announce_autosubmit.changedFilterLabel');
        });

        var submittedFormId = window.localStorage.getItem('announce_autosubmit.submittedFormId');
        if (!submittedFormId ||
          settings.announce_autosubmit.forms[submittedFormId] === undefined) {
          return;
        }

        // Look for changes in form parameters in last submission.
        var referrerPath = document.referrer.split('?')[0];
        var currentPath = document.URL.split('?')[0];
        if (referrerPath !== currentPath) {
          return;
        }

        // submittedFormId could be old, we need to check URL and parameters.
        var referrerParameters = getParameters(document.referrer);
        var currentParameters = getParameters(document.URL);
        var formParameters = settings.announce_autosubmit.forms[submittedFormId].formParameters;

        for (var i in formParameters) {
          var name = formParameters[i].name;
          var announcement = '<p>' +
            Drupal.t('New "@label" filter value applied to results',
              {'@label': formParameters[i].label}) +
            '</p>';

          // Referrer page might not have query parameters.
          if (!referrerParameters) {
            // Then the parameter has changed if is not in its default value.
            if (currentParameters[name] !== undefined &&
              currentParameters[name] !== formParameters[i].defaultValue) {
              announceAndFocus(announcement, name, $('form#' + submittedFormId));
            }
          } else if (!currentParameters) {
            announceAndFocus('<p>' +
              Drupal.t('Filters have been cleared, and default values applied to results') +
              '</p>', name, $('form#' + submittedFormId));
            return;
          } else if (currentParameters[name] !== undefined &&
            currentParameters[name] !== referrerParameters[name]) {
            announceAndFocus(announcement, name, $('form#' + submittedFormId));
          }
        }

        function getParameters(url) {
          var paramString = url.split('?')[1];
          if (paramString) {
            paramString = paramString.split('#')[0];
            var paramsArray = paramString.split('&');
            var parameters = {};
            for (var i = 0; i < paramsArray.length; i++) {
              var pair = paramsArray[i].split('=');
              parameters[pair[0]] = pair[1];
            }
            return parameters;
          }
          return paramString;
        }

        function announceAndFocus(announcement, elementName, context) {
          Drupal.announce(announcement);
          var id = context.find('[name="' + elementName + '"]').first().attr('id');
          if (id) {
            // Ensure focus is triggered after all DOM elements positioning.
            $(document).ready(function() {
              $('#' + id, context).focus();
            });
          }
        }

      });

    }
  };
})(Drupal, jQuery);
